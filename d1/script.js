// let x = 5
// while (x > 10) {
//     console.log(x)
//     x++
// }
// let x = 5
// do {
//     console.log(x)
//     x++
// } while (x > 10)

// let count = 1

// while(count <= 10) {
//     console.log(`Count is ${count}.`)
//     count += 2;
// }

// ****************

// 1. Display in the console all numbers between -10 and 19.
// 2. Display all even numbers 10 and 40.
// 3. Print all odd numbers between 300 and 333.

// *====*
// | #1 |
// *====*
// While loop:
// let count = -10;
// while (count <= 19) {
//     console.log(count)
//     count++
// }

// Do...while loop:
// let count = -10
// do {
//     console.log(count)
//     count++
// } while (count <= 19)

// For loop:
// for (let count = -10; count <= 19; count++)
//     console.log(count)

// *====*
// | #2 |
// *====*
// While loop:
// let count = 10;
// while (count <= 40) {
//     console.log(count)
//     count+=2
// }

// Do...while loop:
// let count = 10;
// do {
//     console.log(count) 
//     console <=40
// } while (count+=2)

// For loop:
// for (let count = 10; count <= 40; count+=2)
//     console.log(count)

// *====*
// | #3 |
// *====*
//While loop:
// let count = 300;
// while (count <= 333) {
//     if (count % 2 === 0){

//     } else
//         console.log(count)
// count++    
// }

// Do...while loop:
// let count = 300;
// do {
//     if (count % 2 === 0){

//     } else
//         console.log(count)
// count++ 
// } while (count <= 333)

// For loop:
// for (let count = 300; count <= 333; count++) {
//     if (count % 2 === 0) {

//     } else
//         console.log(count)
// }

// ****************
// function giveTwoNum (input1, input2) {
//     for (input1; input1 <= input2; input1++)
//         console.log(input1)        
// }

// const result = giveTwoNum (5, 10)
// console.log(result)

// *****************

// Num2 should always be higher than num1 to execute the loop
// If not, print in the console, "Please make sure 1st number is higher than the 2nd number"

// function giveTwoNum (input1, input2) {
//     if (input1 > input2)
//         console.log("ERROR!\nPlease make sure that the first input is higher than the 2nd input!")
//     for (input1; input1 <= input2; input1++)
//             console.log(input1)        
// }
// giveTwoNum (11, 10)

// function giveTwoNum(input1, input2) {
//     if (input1 === input2)
//         console.log("ERROR!!!\nPlease enter two different numbers!")
//     else if (input1 > input2)
//         console.log("ERROR!!!\nPlease make sure that the first input is higher than the second input!")
//     else
//         for (input1; input1 <= input2; input1++)
//             console.log(input1)
// }

// giveTwoNum(8, 10)

// function sample3(input1, input2) {
//     let temp = 0;
//     for(input1; input1 <= input2; input1++){
//         temp = + input1
//     } return temp;
// }

// const total = sample3(3, 5)
// console.log(`Output: ${total}`)

// function sample4(input1, input2, input3) {
//     for(input1; input1 < input2; input1 += input3)
//         console.log(input1)
// }
// sample4(1, 10, 2)

// **************
console.log("*** JS Try-Catch, Continue-Break ***")
//Try-Catch-Finally
// try {
//     notAFunction();
// } catch (err) {
//     console.log(err);
// }

// function determineTyphoonIntensity(windSpeed){
//     if (windSpeed < 30)
//         return 'Not a typhoon yet.'
//     else if (windSpeed <= 61)
//         return 'Tropical Depression detected.'
//     else if (windSpeed >= 62 && windSpeed <= 88)
//         return 'Tropical Storm detected.'
//     else if (windSpeed >= 89 && windSpeed <= 117)
//         return 'Severe Tropical Storm detected.'
//     else
//         return 'Typhoon detected.'
// }

// const typhoonResult = determineTyphoonIntensity(68)
// console.log(typhoonResult)

// function showIntensityAlert(windSpeed){
//     try {
//         alert(determineTyphoonIntensity(windSpeed))
//     } catch (error) {
//         console.warn(typeof error)
//         console.error(error.message)
//     } finally {
//         alert('Intensity update will show new alert.')
//     }
// }

// showIntensityAlert(56)

// ***************
// Continue-Break
// for (let count = 0; count <= 20; count++) {
//     if (count % 2 === 0)
//         continue
//     console.log(`Continue ${count}`)

//     if(count > 10)
//         break;
// }

let name = `Alexandro`
let array = []
for (let i = 0; i < name.length; i++) {
    // console.log(name[i])
    array.push(name[i])

    if (name[i].toLowerCase() === "a") {
        console.log(`Continue to the next iteration. ${name[i]}`)
        continue
    }
    if (name[i] === `d`)
        break
}
console.log(array)