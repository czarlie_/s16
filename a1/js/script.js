// let count = 1

// while(count <= 10) {
//     console.log(`Count is ${count}.`)
//     count += 2;
// }

// 1. Display in the console all numbers between -10 and 19.
// 2. Display all even numbers 10 and 40.
// 3. Print all odd numbers between 300 and 333.

// *====*
// | #1 |
// *====*
// While loop:
// let count = -10;
// while (count <= 19) {
//     console.log(count)
//     count++
// }

// Do...while loop:
// let count = -10
// do {
//     console.log(count)
//     count++
// } while (count <= 19)

// For loop:
// for (let count = -10; count <= 19; count++)
//     console.log(count)

// *====*
// | #2 |
// *====*
// While loop:
// let count = 10;
// while (count <= 40) {
//     console.log(count)
//     count+=2
// }

// Do...while loop:
// let count = 10;
// do {
//     console.log(count) 
//     console <=40
// } while (count+=2)

// For loop:
// for (let count = 10; count <= 40; count+=2)
//     console.log(count)

// *====*
// | #3 |
// *====*
//While loop:
// let count = 300;
// while (count <= 333) {
//     if (count % 2 === 0){

//     } else
//         console.log(count)
// count++    
// }

// Do...while loop:
// let count = 300;
// do {
//     if (count % 2 === 0){

//     } else
//         console.log(count)
// count++ 
// } while (count <= 333)

// For loop:
// for (let count = 300; count <= 333; count++) {
//     if (count % 2 === 0) {

//     } else
//         console.log(count)
// }