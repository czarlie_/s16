//*===================================*
//| Checks if a number is odd or even |
//*===================================*
// function oddEven(input) {
//     let result = (input % 2 === 0) ? console.log(input + " is an even number.") : console.log(input + " is an odd number.")
// }
// console.log(oddEven(9))

//*============================*
//| Checks the day of the week |
//*============================*
// function dayOfTheWeek(input) {
//     if (input === 1) {
//         return 'Monday.'
//     else if (input === 2)
//         return 'Tuesday.'
//     else if (input === 3)
//         return 'Wednesday, my dudes.'
//     else if (input === 4)
//         return 'Thursday.'
//     else if (input === 5)
//         return 'Friday.' 
//     else if (input === 6)
//         return 'Saturday.'
//     else if (input === 7)
//         return 'Sunday.'
//     else
//         return "\n\n*=======================================*\n|                                       |\n| ERROR: You have an invalid input!     |\n| Please Enter a Number from 1 to 7.    |\n|                                       |\n*=======================================*\n\n"
//     }
// }
// let result = dayOfTheWeek(9)
// console.log(`It is: ${result}`)

//*============================*
//| Checks the day of the week |
//| (Switch form)              |
//*============================*
function dayOfTheWeek(input) { 
    switch(input) {
        case 1:
            return 'Monday.';
        case 2:
            return 'Tuesday.';
        case 3:
            return 'Wednesday, my dudes.';
        case 4:
            return 'Thursday.';
        case 5:
            return 'Friday.';
        case 6:
            return 'Saturday.';
        case 7:
            return 'Sunday.';
        default:
            return '\n\n*=======================================*\n|                                       |\n| ERROR: You have an invalid input!     |\n| Please Enter a Number from 1 to 7.    |\n|                                       |\n*=======================================*\n\n'
    }
}

let day = dayOfTheWeek(3);
console.log(`It is ${day}`)